﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Enemy_Instantiator : MonoBehaviour
{
    public GameObject[] puntosReaparicion;
    public GameObject[] enemigos;
    int tiposEnemigos = 1;
    public int score = 0;
    public Text txtScore;
    void Start()
    {
        
    }

    void Update()
    {
        txtScore.text = score.ToString();
    }

    public void GenerarEnemigos()
    {
        int spawn = Random.Range(0, 11);


        Instantiate(enemigos[tiposEnemigos], puntosReaparicion[spawn].transform.position, puntosReaparicion[spawn].transform.rotation);
    }
}
