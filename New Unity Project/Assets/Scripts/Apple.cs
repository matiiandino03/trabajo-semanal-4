﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Apple : MonoBehaviour
{
    public GameObject sword;
    public Transform me;
    public int nailedSwords = 0;
    public Text txtScore;
    private void Update()
    {
        me.Rotate(Vector3.up * 100.0f * Time.deltaTime);
        setSword();
        txtScore.text = nailedSwords.ToString();
    }

    void setSword()
    {
        sword.transform.SetParent(me);
    }
}
