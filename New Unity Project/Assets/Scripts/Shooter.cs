﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooter : MonoBehaviour
{
    public GameObject sword;
    void Start()
    {
        
    }
    void Update()
    {
        if(Input.GetMouseButtonDown(0))
        {
            throwSword();
        }
    }

    void throwSword()
    {
        Instantiate(sword, transform.position, transform.rotation);
    }
}
