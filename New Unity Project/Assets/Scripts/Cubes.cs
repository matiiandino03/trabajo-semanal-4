﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Cubes : MonoBehaviour
{
    public Rigidbody rb;
    public bool isBlue;
    private Renderer rend;
    public bool isOrange = true;

    public Color colorOrange = Color.yellow;
    public Color colorBlue = Color.cyan;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        rend = GetComponent<Renderer>();
    }

    void Update()
    {
        
    }

    public void FixedUpdate()
    {
        if(isBlue)
        {
            rb.AddForce(new Vector3(0, 8f, 0));
        }

        else if(isOrange)
        {
            rb.AddForce(new Vector3(0, -8f, 0));
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Finish" && !isBlue)
        {
            SceneManager.LoadScene("Game 34");
        }
    }

    private void OnMouseDown()
    {
        if(isBlue)
        {
            rend.material.color = colorOrange;
            isBlue = !isBlue;
            isOrange = !isOrange;
        }

        else if (isOrange)
        {
            rend.material.color = colorBlue;
            isBlue = !isBlue;
            isOrange = !isOrange;
        }
    }

}


