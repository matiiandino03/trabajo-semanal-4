﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class Timer : MonoBehaviour
{
    private float StartTime;
    public Text txtTimer;
    void Start()
    {
        StartTime = Time.time;
    }
    void Update()
    {
        float TimerControl = Time.time - StartTime;
        string mins = ((int)TimerControl / 60).ToString("00");
        string segs = (TimerControl % 60).ToString("00");
        string milisegs = ((TimerControl * 100) % 100).ToString("00");

        string TimerString = string.Format("{00}:{01}:{02}", mins, segs, milisegs);        
        txtTimer.text = TimerString.ToString();

        Invoke("timeOut", 29.01f);
    }

    void timeOut()
    {
        SceneManager.LoadScene("FreeGame");
    }
}