﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{

    public Enemy_Instantiator enemyInstantiator;
    void Start()
    {
        enemyInstantiator = FindObjectOfType<Enemy_Instantiator>();        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnMouseDown()
    {
        enemyInstantiator.score++;
        enemyInstantiator.GenerarEnemigos();
        Destroy(this.gameObject);
    }
}
