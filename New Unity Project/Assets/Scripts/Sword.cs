﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Sword : MonoBehaviour
{
    public Sword swordScript;
    public float velocidad;
    void Start()
    {
        swordScript = this.GetComponent<Sword>();
    }
    private void FixedUpdate()
    {
        transform.position += transform.up * velocidad * Time.fixedDeltaTime;
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Apple")
        {
            other.GetComponent<Apple>().sword = this.gameObject;
            other.GetComponent<Apple>().nailedSwords += 1;
            swordScript.enabled = false;
        }

        if (other.tag == "Sword")
        {
            SceneManager.LoadScene("Game 13");
        }
    }

}
