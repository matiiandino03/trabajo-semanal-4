﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlPlataforma : MonoBehaviour
{
    public float runSpeed = 2;
    // Start is called before the first frame update



    void Update()
    {
        if (Input.GetKey("d") || Input.GetKey("right"))
            transform.Rotate(-Vector3.back, -runSpeed * Time.deltaTime);

        else if (Input.GetKey("a") || Input.GetKey("left"))
            transform.Rotate(Vector3.forward, runSpeed * Time.deltaTime);




    }
    
}
