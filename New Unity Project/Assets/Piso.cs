﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Piso : MonoBehaviour
{

    private void OnCollisionEnter(Collision collision)
    {

        if (collision.transform.CompareTag("Player"))
        {
            Debug.Log("Player Damaged");
            SceneManager.LoadScene(3);

        }

    }
}
